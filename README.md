# OCR Software For Malayalam 

<p>This is a Malayalam OCR Software. OCR takes in a document image and renders the text in the image. This OCR reads and transcripts Malayalam and English.</p>

## Libraries Used

NodeJS : Electron, zerorpc. 
<br>
Opencv.js is also used, without integrating it with node.
<br>
Python3: numpy, Keras, Pillow, zerorpc.

### Steps for Execution
Clone this repo.
#### For Python3 Part
 * Create a virtual environment for python3.
 * Install the above said python packages.
#### For NodeJS Part
 * Run the installation by setting up ```npm install```.
 * ```npm install electron-rebuild && ./node_modules/.bin/electron-rebuild```  : Helps to rebuild zeromq binaries to the required electron version.
 * Change Line-32 and Line-38 in `main.js` to suit your path to `api.py` and python interpreter.
 * Run ```npn start``` for starting the OCR application.
#### Tips
 * Double click on the image area inside the app to toggle between zoom and drag mode.
 * tcp requests from electron to python process has a timeout of 10 seconds. So image-docs with a lot of words to process might return a heartbeat-error. One solution would be to edit zmq node module config files manually to extend/cancel timeout functions.


### Authors:
 * Deepu Shaji, Research Assistant ICFOSS
