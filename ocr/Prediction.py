# Sree RajaRajeshwari OCR
# Created and Maintained by Deepu Shaji @ ICFOSS <deepushaji@icfoss.in>
import itertools
from PIL import Image
import os
import numpy as np


def real(img,j,h,w,b,model,char_vector,typ):
	try:
		if typ == "mal_model":
			img_w = 400
		elif typ == "eng_model":
			img_w = 400
		else:
			img_w = 400		
		ar = float(h)/w
		new_wid = int(32/ar)
		img = Image.fromarray(img)
		real_img = img.resize((new_wid, 32), Image.ANTIALIAS)
		if real_img.size[0] > img_w:
			real_img = real_img.resize((img_w, 32))
		img = np.array(real_img)
		img1 = img.astype(np.float32)
		var = new_wid
		img1 = (img1 / 255.0) * 2.0 - 1.0
		img_pred = img1.T
		img_pred = np.expand_dims(img_pred, axis=-1)
		img_pred = np.expand_dims(img_pred, axis=0)
		X_data = np.ones([img_w, 32, 1])
		X_data[:img_pred.shape[1]] = img_pred
		img_pred = np.expand_dims(X_data, axis=0)
		out = model.predict(img_pred)
		letters = [letter for letter in char_vector]
		out_best = list(np.argmax(out[0, 2:], axis=1))
		out_best = [k for k, g in itertools.groupby(out_best)]
		
		outstr = ''
		for i in out_best:
			if i < len(letters):
				outstr += letters[i]
		real_img.convert("L").save(os.path.dirname(os.path.abspath(__file__))+"/testing/"+outstr.replace("/","=")+'.jpg', 'JPEG')
		#g.write(str(var)+ " = "+outstr+'-'+str(j)+"-"+str(h)+"-"+str(w)+'\n')
	except Exception as e:
		return str(e)
	return outstr


