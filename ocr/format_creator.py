# Sree RajaRajeshwari OCR
# Created and Maintained by Deepu Shaji @ ICFOSS <deepushaji@icfoss.in>
import Prediction as p


def run_recognition(contour_arrays,image,thresh,model,char_vector,typ):

	global_hash = []
	def check_and_append(x,y,w,h,img):
		if global_hash == []:
			global_hash.append({'sh':y,'se':y+h,'img':[img]})
		else:
			largest = 0
			key = 0
			for i,x in enumerate(global_hash):

				num = len(set(list(range(x['sh'],x['se']))).intersection(set(list(range(y,y+h)))))/len(set(list(range(y,y+h))))

				if num > largest:
					largest = num
					key = i
			if largest == 0:
				global_hash.append({'sh':y,'se':y+h,'img':[img]})
			else:
				global_hash[key]['img'].append(img)


	j = thresh


	for i,cnt in enumerate(contour_arrays):
		x = int(cnt[0])
		y = int(cnt[1])
		w = int(cnt[2])
		h = int(cnt[3])
		w1 = int(cnt[4])
		h1 = int(cnt[5])
		re = image[y:y+h,x+int(j/2):x+w-int(j/2)]
		string = p.real(re,i,h,w,i,model,char_vector,typ)
		check_and_append(x,y,w1,h1,[i,string,x])


	c =sorted(global_hash, key=lambda k: k['sh'])
	#g.close()
	line = ''
	for h in c:	
		print(h['img'])
		sort =sorted(h['img'], key=lambda k: k[2])
		print(sort)
		for word in sort:
			line = line + word[1] + ' '
		line = line + '\n'
	return line

