
MAL_VECTOR = 'ംഃഅആഇഈഉഊഋഌഎഏഐഒഓഔകഖഗഘങചഛജഝഞടഠഡഢണതഥദധനഩപഫബഭമയരറലളഴവശഷസഹാിീുൂൃെേൈൊോൌ്ൎൗൺൻർൽൾ'

ASCII_VECTOR = '-+=!@#$%^&*(){}[]|\'"\\,./?<>;:0123456789'

ENG_VECTOR = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789'


CHAR_VECTOR = ENG_VECTOR#MAL_VECTOR#+ASCII_VECTOR                                                                      

letters = [letter for letter in CHAR_VECTOR]
print(letters)
num_classes = len(letters) + 1

img_w, img_h =400, 32

# Network parameters
batch_size = 64
val_batch_size = 16

downsample_factor = 4
max_text_len = 45
